﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MICZ.CodeBuilder.IRepository.SqlSugar;

namespace MICZ.CodeBuilder.Manage.Controllers
{

    public class HomeController : BaseController
    {

        /// <summary>
        /// 后台管理首页
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        //[ServiceFilter(typeof(LoginFilterAttribute))]
        public IActionResult Index()
        {
            //DbContextRepository.GetByIds(1);
            //userinfoService.GetList(0,0,"",a=>true);
            return View();
        }

        /// <summary>
        /// 后台管理默认页
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        //[ServiceFilter(typeof(LoginFilterAttribute))]
        public IActionResult Default()
        {
            //DbContextRepository.GetByIds(1);
            //userinfoService.GetList(0,0,"",a=>true);
            return View();
        }
    }
}
