﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MICZ.CodeBuilder.Commons.DataEnum;

namespace MICZ.CodeBuilder.Manage.Controllers
{
    public class BuilderController : BaseController
    {
        /// <summary>
        /// 生成信息
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult Index()
        {
            ViewData["title"] = "生成信息";
            return View();
        }

        /// <summary>
        /// Controller生成信息
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult Controller()
        {
            ViewData["title"] = "Controller生成信息";
            return View();
        } 

        /// <summary>
        /// App生成信息
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult App()
        {
            ViewData["title"] = "App生成信息";
            return View();
        } 
        
        /// <summary>
        /// Service生成信息
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult Service()
        {
            ViewData["title"] = "Service生成信息";
            return View();
        } 
        
        /// <summary>
        /// Model生成信息
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult Model()
        {
            ViewData["title"] = "Model生成信息";
            return View();
        }

        /// <summary>
        /// Repository生成信息
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult Repository()
        {
            ViewData["title"] = "Repository生成信息";
            return View();
        }

        /// <summary>
        /// Register生成信息
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult Register()
        {
            ViewData["title"] = "Register生成信息";
            return View();
        }

        /// <summary>
        /// AutoMapper
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public IActionResult AutoMapper()
        {
            ViewData["title"] = "AutoMapper生成信息";
            return View();
        }

    }
}