﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using MICZ.CodeBuilder.Commons.ConfigExtensions;
using SqlSugar;
using System.Dynamic;
using Newtonsoft.Json;
using MICZ.CodeBuilder.Commons.JsonExtensions;
using System.Text.RegularExpressions;
using MICZ.CodeBuilder.IRepository.SqlSugar;

namespace MICZ.CodeBuilder.ManageApi.Controllers
{
    
    public class HomeController : BaseController
    {
        ISqlSugarRepository SqlSugarRepository;
        public HomeController(ISqlSugarRepository _SqlSugarRepository)
        {
            SqlSugarRepository = _SqlSugarRepository;
        }

        /// <summary>
        /// 数据库Table属相结构
        /// </summary>
        /// <returns></returns>
        [EnableCors]
        public async Task<IActionResult> TableList()
        {
            List<dynamic> dynamics = new List<dynamic>();
            foreach (var item in SqlSugarRepository.Db.DbMaintenance.GetTableInfoList())
            {
                dynamic _dynamic = new
                {
                    TableName = item.Name,
                    Description = item.Description,
                    Type = item.DbObjectType,
                };
                dynamics.Add(_dynamic);
            }
            dynamic obj = new MICZ.CodeBuilder.Commons.DynamicExtensions.DynamicObjectHelper();
            obj.set("Obj", dynamics);
            JsonDataModel.Status = 1;
            JsonDataModel.Data = obj;
            return new JsonResult(JsonDataModel);
        }

        /// <summary>
        /// 获取指定Table列
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>
        [EnableCors]
        public async Task<IActionResult> TableColumnsList(string TableName)
        {
            if (string.IsNullOrWhiteSpace(TableName)) {
                JsonDataModel.Msg = "参数为空";
                return new JsonResult(JsonDataModel);
            }
            List<dynamic> dynamics = new List<dynamic>();
            foreach (var item in SqlSugarRepository.Db.DbMaintenance.GetColumnInfosByTableName(TableName, false))
            {
                dynamic _dynamic = new
                {
                    ColumnsName = item.DbColumnName,
                    Description = item.ColumnDescription,
                    DataType = item.DataType,
                    Length = item.Length,
                    DefaultValue = item.DefaultValue,
                    IsPrimarykey = item.IsPrimarykey,//是否是主键
                    IsIdentity = item.IsIdentity,//是否自增
                    IsNullable = item.IsNullable,//是否可以为Null
                };
                dynamics.Add(_dynamic);
            }
            dynamic obj = new MICZ.CodeBuilder.Commons.DynamicExtensions.DynamicObjectHelper();
            obj.set("Obj", dynamics);
            JsonDataModel.Status = 1;
            JsonDataModel.Data = obj;
            JsonDataModel.TotalCount = dynamics.Count();
            return new JsonResult(JsonDataModel);
        }
    }
}
