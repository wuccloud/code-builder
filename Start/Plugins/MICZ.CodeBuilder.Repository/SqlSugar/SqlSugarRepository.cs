﻿using Microsoft.Extensions.Configuration;
using MICZ.CodeBuilder.Commons.ConfigExtensions;
using SqlSugar;
using System;
using System.Linq;
using MICZ.CodeBuilder.IRepository.SqlSugar;

namespace MICZ.CodeBuilder.Repository.SqlSugar
{
    public class SqlSugarRepository:ISqlSugarRepository
    {
        public SqlSugarClient Db { get; set; }
        /// <summary>
        /// 实现SqlSugar对象的仓库(Repository)
        /// </summary>
        /// <returns></returns>
        public SqlSugarRepository()
        {
            Db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = ConfigExtensions.Configuration.GetConnectionString("TestConnection"),
                DbType =DbType.SqlServer,
                InitKeyType = InitKeyType.Attribute,//从特性读取主键和自增列信息
                IsAutoCloseConnection = true,//开启自动释放模式和EF原理一样我就不多解释了

            }) ;
            //Db.DbFirst.CreateClassFile("c:\\Demo\\1", "test");//生成实体模型Model
            //用来打印Sql方便你调式    
            Db.Aop.OnLogExecuting = (sql, pars) =>
            {
                Console.WriteLine(sql + "\r\n" +
                Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                Console.WriteLine();
            };
            //return Db;
        }
        //public IDbContextRepository<Userinfo> UserinfoDb()
        //{ 
        //return new DbContextRepository<Userinfo>(Db); //用来处理Userinfo表的常用操作
        //}

        //public IDbContext<Email_List> Email_ListDb { get { return new DbContext<Email_List>(Db); } }//用来处理Email_List表的常用操作
    }
}
