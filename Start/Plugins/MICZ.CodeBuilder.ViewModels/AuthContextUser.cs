﻿using MICZ.CodeBuilder.Commons.JwtExtensions;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MICZ.CodeBuilder.ViewModels
{
    /// <summary>
    /// 获取当前用户信息
    /// </summary>
    public static class AuthContextUser
    {

        /// <summary>
        /// 用户Id
        /// </summary>
        public static string USERID
        {
            get
            {
                return JwtHelper.USERID;
            }
        }

        /// <summary>
        /// ID
        /// </summary>
        public static string ID
        {
            get
            {
                return JwtHelper.ID;
            }
        }


        //// <summary>
        //// 当前用户信息
        //// </summary>
        //public static CurrentUser CurrentUser
        //{
        //    get
        //    {
        //        CurrentUser user = null;
        //        string userId = Current.User.FindFirstValue("UserId");
        //        if (string.IsNullOrEmpty(userId))
        //        {
        //            return null;
        //        }
        //        YuebonCacheHelper cacheHelper = new YuebonCacheHelper();
        //        var userObject = cacheHelper.Get(ComnCacheName.Cache_ManagerCurrentUser + "_" + userId);
        //        if (userObject != null)
        //        {
        //            user = JsonConvert.DeserializeObject<CurrentUser>(userObject.ToString());
        //            //user =(CurrentUser)userObject;
        //        }
        //        else
        //        {
        //            //缓存中不存在用户信息
        //            return null;
        //        }
        //        return user;
        //    }
        //}

        /// <summary>
        /// 当前用户信息
        /// </summary>
        public static dynamic CurrentUser
        {
            set;
            get;
        }
    }
}
