﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.ViewModels
{
    public class WorkContent
    {
        /// <summary>
        /// 区域名称
        /// </summary>
        public string AreasName = "";
        /// <summary>
        /// 控制器名称
        /// </summary>
        public string ControllerName = "";
        /// <summary>
        /// 动作方法名称
        /// </summary>
        public string ActionName = "";
        /// <summary>
        /// 访问根目录
        /// </summary>
        public string CurrentPath = "";
        /// <summary>
        /// 主题API路径
        /// </summary>
        public string ThemeAPI = "";
        /// <summary>
        /// 当前主题路径(引用静态文件使用)
        /// </summary>
        public string ThemePath = "";
        /// <summary>
        /// 页面开始执行时间
        /// </summary>
        public DateTime StartExecuteTime;
        /// <summary>
        /// 页面执行时间毫秒
        /// </summary>
        public double EndExecuteTime = 0;
    }
}
