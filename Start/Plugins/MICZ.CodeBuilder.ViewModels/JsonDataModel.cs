﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.ViewModels
{
    public class JsonDataModel
    {
        private int _Status = 0;
        /// <summary>
        ///  处理状态 0:失败  1:成功
        /// </summary>
        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private string _Msg = "";
        /// <summary>
        /// 处理结果信息
        /// </summary>
        public string Msg
        {
            get { return _Msg; }
            set { _Msg = value; }
        }

        private dynamic _Data = new MICZ.CodeBuilder.Commons.DynamicExtensions.DynamicObjectHelper();
        /// <summary>
        /// 动态数据，具体业务逻辑使用时  dynamic obj=new TTvCore.Common.Helper.DynamicObjectHelper(); obj.set(string PropertyName,object value);  data=obj;进行赋值。
        /// </summary>
        public dynamic Data
        {
            get
            {
                if (_Data.GetMemberCount() > 0)
                {
                    return _Data;
                }
                return "";
            }
            set { _Data = value; }
        }

        private string _Html = "";
        /// <summary>
        /// Html 代码
        /// </summary>
        public string Html
        {
            get { return _Html; }
            set { _Html = value; }
        }
        private int _Page = 0;
        /// <summary>
        /// 当前页面
        /// </summary>
        public int Page
        {
            get { return _Page; }
            set { _Page = value; }
        }
        private int _limit = 20;
        /// <summary>
        /// 页面数量
        /// </summary>
        public int limit
        {
            get { return _limit; }
            set { _limit = value; }
        }
        private int _totalCount = 0;
        /// <summary>
        /// 总数据量
        /// </summary>
        public int TotalCount
        {
            get { return _totalCount; }
            set { _totalCount = value; }
        }
    }
}
