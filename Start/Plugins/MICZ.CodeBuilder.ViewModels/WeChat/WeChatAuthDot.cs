﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.ViewModels.WeChat
{
    public class WeChatAuthDot
    {

        /// <summary>
        /// 用户唯一标识
        /// </summary>
        public string openid { set; get; }

        /// <summary>
        /// 会话密钥
        /// </summary>
        public string session_key { set; get; }

        /// <summary>
        /// 用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回，详见 UnionID 机制说明。
        /// </summary>
        public string unionid { set; get; }

        /// <summary>
        /// 错误码
        /// </summary>
        public string errcode { set; get; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string errmsg { set; get; }
    }
}
