﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.Commons.RandomExtensions
{
    public static class RandomExtensions
    {
        /// <summary>
        /// 设置随机ID(Random生存Int返回)
        /// </summary>
        /// <param name="len">长度</param>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns></returns>
        public static int GuidInt(int len = 5, int min = 0, int max = 99999999)
        {
            Random random = new Random(len);
            return random.Next(min, max);
        }
        /// <summary>
        /// 设置随机ID(Random生成String返回)
        /// </summary>
        /// <param name="len">长度</param>
        /// <param name="q">前缀</param>
        /// <param name="h">后缀</param>
        /// <returns></returns>
        public static string GuidIntToString(int len = 10, string q = "", string h = "")
        {
            Random random = new Random(len);
            return q + random.Next(0, (len * 200)) + h;
        }
        /// <summary>
        /// 设置随机ID(Guid生成String返回)
        /// </summary>
        /// <param name="len">长度</param>
        /// <param name="q">前缀</param>
        /// <param name="h">后缀</param>
        /// <returns></returns>
        public static string GuidString(int len = 10, string q = "", string h = "")
        {
            string guid = Guid.NewGuid().ToString("N");
            string r = q + guid + h;
            if (r.Length > len)
            {
                r = r.Substring(q.Length + 1, len);
            }
            if (r.Length < len)
            {
                int ins = len - r.Length;
                do
                {
                    string guid1 = Guid.NewGuid().ToString("N");
                    if (guid1.Length >= ins)
                    {
                        r = r.Insert(q.Length + 1, guid1.Substring(0, ins));
                        ins = 0;
                    }
                    else
                    {
                        r = r.Insert(q.Length + 1, guid1);
                        ins = ins - guid1.Length;
                    }
                } while (ins == 0);
            }

            return r;
        }
    }
}
