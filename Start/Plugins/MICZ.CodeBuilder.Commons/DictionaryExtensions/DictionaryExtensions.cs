﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.Commons.DictionaryExtensions
{
    public static class DictionaryExtensions
    {
        /// <summary>
        /// 获取字典的对应key的值ByString
        /// </summary>
        /// <param name="dic">字典数据</param>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static bool GetValueByBool(this Dictionary<string, string> dic, string key)
        {
            bool value = false;
            if (dic == null)
            {
                return false;
            }
            string dicVal = "";
            dic.TryGetValue(key, out dicVal);
            bool.TryParse(dicVal, out value);
            return value;
        }
        /// <summary>
        /// 获取字典的对应key的值ByString
        /// </summary>
        /// <param name="dic">字典数据</param>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static string GetValueByString(this Dictionary<string, string> dic, string key, string defaultValue = "")
        {
            string value = "";
            if (dic == null)
            {
                return defaultValue;
            }
            dic.TryGetValue(key, out value);
            if (string.IsNullOrEmpty(value))
            {
                value = defaultValue;
            }
            return value;
        }
        /// <summary>
        /// 获取字典的对应key的值ByArray
        /// </summary>
        /// <param name="dic">字典数据</param>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static string[] GetValueByArray(this Dictionary<string, string> dic, string key)
        {
            string[] value = new string[] { };
            if (dic == null)
            {
                return new string[] { };
            }
            string dicVal = "";
            dic.TryGetValue(key, out dicVal);
            value = dicVal.Split(",");
            //value=JsonConvert.DeserializeObject<string[]>(dicVal);
            if (value.Length==0)
            {
                value = new string[] { };
            }
            return value;
        }
        /// <summary>
        /// 获取字典的对应key的值ByDateTime
        /// </summary>
        /// <param name="dic">字典数据</param>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static DateTime GetValueByDateTime(this Dictionary<string, string> dic, string key)
        {
            string value = "";
            dic.TryGetValue(key, out value);         
            DateTime Val = DateTime.Now;
            if (string.IsNullOrEmpty(value))
            {
                return Val;
            }
            DateTime.TryParse(value, out Val);
            return Val;
        }

        /// <summary>
        /// 获取字典的对应key的值ByDateTime
        /// </summary>
        /// <param name="dic">字典数据</param>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static DateTime GetValueByDateTime(this Dictionary<string, string> dic, string key, DateTime defaultValue)
        {
            string value = "";
            if (dic == null)
            {
                return defaultValue;
            }
            dic.TryGetValue(key, out value);
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }
            DateTime Val = DateTime.Now;
            DateTime.TryParse(value, out Val);
            return Val;
        }

        /// <summary>
        /// 获取字典的对应key的值ByInt
        /// </summary>
        /// <param name="dic">字典数据</param>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static int GetValueByInt(this Dictionary<string, string> dic, string key, int defaultValue = 0)
        {
            if (dic == null)
            {
                return defaultValue;
            }
            string value = defaultValue.ToString();
            dic.TryGetValue(key, out value);
            if (value == null)
            {
                value = defaultValue.ToString();
            }
            int iVal = 0;
            if (value == "on")
            {
                value = "1";
            }
            int.TryParse(value, out iVal);
            return iVal;
        }

        /// <summary>
        /// 获取字典的对应key的值ByDecimal
        /// </summary>
        /// <param name="dic">字典数据</param>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static decimal GetValueByDecimal(this Dictionary<string, string> dic, string key, decimal defaultValue = 0m)
        {
            if (dic == null)
            {
                return defaultValue;
            }
            string value = "0.00";
            dic.TryGetValue(key, out value);
            decimal iVal = 0m;
            decimal.TryParse(value, out iVal);
            return iVal;
        }
    }
}
