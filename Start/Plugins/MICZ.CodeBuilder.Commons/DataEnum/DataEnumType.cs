﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MICZ.CodeBuilder.Commons.DataEnum
{
    public class DataEnumType {
        public enum DataEnum
        {
            [Description("显示")]
            Show = 1,
            [Description("显示一条")]
            Get = 2,
            [Description("列表")]
            List = 3,
            [Description("添加")]
            Add = 4,
            [Description("修改")]
            Edit = 5,
            [Description("更新")]
            Update = 6,
            [Description("删除")]
            Delete = 7,
            [Description("审核")]
            Auth = 8
        }
    }
}
