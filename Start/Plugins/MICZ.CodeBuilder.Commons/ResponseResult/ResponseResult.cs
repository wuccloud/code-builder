﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MICZ.CodeBuilder.Commons.Comn;

namespace MICZ.CodeBuilder.Commons.ResponseResult
{
    /// <summary>
    /// 数据返回类型
    /// </summary>
    public class ResponseResult
    {


        /// <summary>
        ///  响应成功消息封装
        /// </summary>       
        /// <param name="returnData">返回数据</param>
        /// <returns></returns>
        public static dynamic Success(object returnData = null)
        {
            return new
            {
                Status = true,
                Code = ReturnErrorCode.RequestSuccessfulCode,
                Msg = ReturnErrorCode.RequestSuccessful,
                ResData = returnData,
            }; 
        }

        /// <summary>
        /// 响应失败消息封装
        /// </summary>
        /// <param name="code">失败代码（参考ReturnErrorCode类）</param>
        /// <param name="msg">失败描述（参考ReturnErrorCode类）</param>
        /// <returns></returns>
        public static dynamic Error(string code, string msg)
        {
            return new 
            {
                Status = false,
                Code = code,
                Msg = msg,
                //ResData = null,
            };
        }


        /// <summary>
        /// 将返回数据转换成HttpResponseMessaged的Json格式
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static HttpResponseMessage toJson(Object obj, HttpStatusCode status = HttpStatusCode.OK)
        {
            String str;
            if (obj is String || obj is Char)
            {
                str = obj.ToString();
            }
            else
            {
                str = JsonConvert.SerializeObject(obj);
            }
            return new HttpResponseMessage
            {
                Content = new StringContent(str, Encoding.GetEncoding("UTF-8"), "application/json"),
                StatusCode = status
            };
        }


        /// <summary>
        /// toJson
        /// </summary>
        /// <param name="obj"></param>      
        /// <returns></returns>
        public static string toJsonString(Object obj)
        {
            String str;
            if (obj is String || obj is Char)
            {
                str = obj.ToString();
            }
            else
            {
                str = JsonConvert.SerializeObject(obj);
            }
            return str;
        }


        //public static IActionResult ToJson(object obj)
        //{
        //    return ContentResult(obj.toJson());
        //}
    }
}
