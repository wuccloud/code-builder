﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace MICZ.CodeBuilder.Commons.VerifyCode
{

    public static class VerifyCode 
    {
        //public IActionResult Base64()
        //{
        //    string base64 = "data:image/png;base64," + Encoding.Default.GetString(DarwingVerifyCode());
        //    return Content(base64);
        //}
        //public IActionResult Img()
        //{
        //    byte[] bytes = DarwingVerifyCode();
        //    return new FileContentResult(bytes, "image/png"); ;
        //}
        //private byte[] DarwingVerifyCode()
        //{
        //    string verifyCode = FY.DLL.Command.RandomExtensions.GuidString(4);
        //    return DarwingVerifyCode(verifyCode);
        //}
        /// <summary>
        /// 画验证码
        /// </summary>
        /// <param name="code">验证码</param>
        /// <param name="width">宽</param>
        /// <param name="height">高</param>
        /// <returns></returns>
        public static byte[] DarwingVerifyCode(string verifyCode, int width = 200, int height = 100)
        {
            //this.HttpContext.Session.SetString("VerifyCode",verifyCode);
            using (Bitmap bit = new Bitmap(width, height))
            using (Graphics graph = Graphics.FromImage(bit))
            {

                Random rand = new Random();
                //graph.Clear(GetRandomLightColor());
                graph.Clear(Color.FromArgb(0,0,0));
                Font font = new Font("Arial", 40, (FontStyle.Bold | FontStyle.Italic));
                Brush brush;
                SizeF totalSizeF = graph.MeasureString(verifyCode, font);
                SizeF curCharSizeF;
                PointF startPointF = new PointF(0, (height - totalSizeF.Height) / 2);
                for (int i = 0; i < verifyCode.Length; i++)
                {
                    brush = new LinearGradientBrush(new Point(0, 0), new Point(1, 1), GetRandomLightColor(), GetRandomLightColor());
                    graph.DrawString(verifyCode[i].ToString(), font, brush, startPointF);
                    curCharSizeF = graph.MeasureString(verifyCode[i].ToString(), font);
                    startPointF.X += curCharSizeF.Width;
                }

                //画图片的干扰线  
                for (int i = 0; i < 10; i++)
                {
                    int x1 = new Random().Next(width);
                    int x2 = new Random().Next(width);
                    int y1 = new Random().Next(height);
                    int y2 = new Random().Next(height);
                    graph.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
                }

                //画图片的前景干扰点  
                for (int i = 0; i < 100; i++)
                {
                    int x = new Random().Next(width);
                    int y = new Random().Next(height);
                    bit.SetPixel(x, y, GetRandomLightColor());
                }
                graph.DrawRectangle(new Pen(Color.Silver), 0, 0, width - 1, height - 1); //画图片的边框线  
                graph.Dispose();
                //保存图片数据  
                MemoryStream ms = new MemoryStream();
                bit.Save(ms, ImageFormat.Jpeg);
                //输出图片流  
                return ms.ToArray();
            }
        }
        /// <summary>
        /// 随机RGB
        /// </summary>
        /// <returns></returns>
        private static Color GetRandomLightColor()
        {
            int low = 180, high = 255;
            int nRend = new Random().Next(high) % (high - low) + low;
            int nGreen = new Random().Next(high) % (high - low) + low;
            int nBlue = new Random().Next(high) % (high - low) + low;
            return Color.FromArgb(nRend, nGreen, nBlue);
        }
    }
}
        