﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MICZ.CodeBuilder.Commons.SessionExtensions
{
    public static class SessionExtensions
    {
        /// <summary>
        /// 获取会话ID
        /// </summary>
        /// <returns></returns>
        public static string GetID(this HttpContext httpContext)
        {
            string BrowserSid = "";
            try
            {
                BrowserSid = GetString(httpContext, "BrowserSid");
                if (string.IsNullOrEmpty(BrowserSid))
                {
                    BrowserSid = httpContext.Session.Id.Replace("-", "").ToUpper();// .Current.Session.Id.Replace("-", "").ToUpper();
                    SetString(httpContext, "BrowserSid", BrowserSid);
                }
            }
            catch
            { }
            return BrowserSid;
        }
        /// <summary>
        /// 清空session
        /// </summary>
        public static void Clear(this HttpContext httpContext)
        {
            try
            {
                httpContext.Session.Clear();
            }
            catch
            { }
        }
        /// <summary>
        /// 设置session
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public static void SetString(this HttpContext httpContext, string key, string value)
        {
            try
            {
                byte[] bArr = Encoding.UTF8.GetBytes(value);
                httpContext.Session.Set(key, bArr);
            }
            catch
            { }
        }
        /// <summary>
        /// 获取session
        /// </summary>
        /// <param name="key">key</param>
        public static string GetString(this HttpContext httpContext, string key)
        {
            try
            {
                byte[] bArr = new byte[] { };
                httpContext.Session.TryGetValue(key, out bArr);
                if (bArr == null)
                {
                    return "";
                }
                return Encoding.UTF8.GetString(bArr);
            }
            catch
            { }
            return "";
        }
        /// <summary>
        /// 设置session
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public static void SetObjectByJson<T>(this HttpContext httpContext, string key, T value)
        {
            SetString(httpContext, key, JsonConvert.SerializeObject(value));
        }
        /// <summary>
        /// 设置session
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public static void SetObject(this HttpContext httpContext, string key, string value)
        {
            SetString(httpContext, key, value);
        }

        /// <summary>
        /// 获取session
        /// </summary>
        /// <param name="key">key</param>
        public static T GetObject<T>(this HttpContext httpContext, string key)
        {
            var value = GetString(httpContext, key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
