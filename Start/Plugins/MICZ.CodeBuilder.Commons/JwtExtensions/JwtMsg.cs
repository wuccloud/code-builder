﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MICZ.CodeBuilder.Commons.JwtExtensions
{
    /// <summary>
    /// JWT对象
    /// </summary>
    public class JwtMsg
    {
        /// <summary>
        /// 到期时间
        /// </summary>
        public string ExpireTime { get; set; }

        /// <summary>
        /// JWT信息
        /// </summary>
        public string JwtStr { get; set; }
    }
}
