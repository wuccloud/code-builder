﻿/***************************************************
 * 模块名称：通用返回错误码
 * 功能概述：通用返回错误码
 * 创建人：李洪亮
 * 创建时间：2019-06-12
 * 修改人：
 * 修改时间：
 * 修改描述：
 *************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MICZ.CodeBuilder.Commons.Comn
{
    /// <summary>
    /// 页面返回代码描述
    /// </summary>
    public class ReturnErrorCode
    {

        #region 请求是否成功失败

        /// <summary>
        /// 请求成功
        /// </summary>
        public static string RequestSuccessful = "请求成功!";
        /// <summary>
        /// 请求成功代码(1)
        /// </summary>
        public static string RequestSuccessfulCode = "1";


        /// <summary>
        /// 请求失败
        /// </summary>
        public static string RequestFailure = "请求失败!";
        /// <summary>
        /// 请求失败代码(0)
        /// </summary>
        public static string RequestFailureCode = "0";

        #endregion

        #region 参数判断
        /// <summary>
        /// 参数不能为空
        /// </summary>
        public static string ParameterEmpty = "参数不能为空!";
        /// <summary>
        /// 参数不能为空 代码
        /// </summary>
        public static string ParameterEmptyCode = "41006";



        /// <summary>
        /// 传递参数出现错误
        /// </summary>
        public static string ParameterError = "传递参数出现错误!";
        /// <summary>
        /// 传递参数出现错误代码
        /// </summary>
        public static string ParameterErrorCode = "40007";


        /// <summary>
        /// 代码不能为空
        /// </summary>
        public static string CodeEmpty = "参数不能为空!";
        /// <summary>
        /// 代码不能为空 代码
        /// </summary>
        public static string CodeEmptyCode = "41007";

        /// <summary>
        /// 代码不能重复
        /// </summary>
        public static string CannotRepeat = "编码不能重复!";
        /// <summary>
        /// 代码不能重复 代码
        /// </summary>
        public static string CannotRepeatCode = "41008";

        /// <summary>
        /// 代码不能重复
        /// </summary>
        public static string CannotRepeatplus = "数据不能重复添加!";
        /// <summary>
        /// 代码不能重复 代码
        /// </summary>
        public static string CannotRepeatplusCode = "41010";
        #endregion

        #region 验证码判断
        /// <summary>
        /// 验证码不能为空
        /// </summary>
        public static string YzmCodeEmpty = "验证码不能为空!";
        /// <summary>
        /// 验证码不能为空 代码
        /// </summary>
        public static string YzmCodeEmptyCode = "41003";


        /// <summary>
        /// 验证码错误
        /// </summary>
        public static string YzmCodeError = "验证码错误!";
        /// <summary>
        /// 验证码错误 代码
        /// </summary>
        public static string YzmCodeErrorCode = "41004";

        /// <summary>
        /// 验证码过期(验证码过期或失效，请重新刷新!)
        /// </summary>
        public static string YzmCodeInvalid = "验证码过期或失效，请重新刷新!";
        /// <summary>
        /// 验证码过期 代码
        /// </summary>
        public static string YzmCodeInvalidCode = "41009";
        #endregion

        #region 手机号
        /// <summary>
        /// 该手机号不能为空
        /// </summary>
        public static string PhoneEmpty = "手机号不能为空!";
        /// <summary>
        /// 该手机号不能为空 代码
        /// </summary>
        public static string PhoneEmptyCode = "40008";


        /// <summary>
        /// 该手机号已存在
        /// </summary>
        public static string PhoneExist = "手机号已存在，请登录或重新注册!";
        /// <summary>
        /// 该手机号已存在代码
        /// </summary>
        public static string PhoneExistCode = "50006";
        #endregion

        #region 用户判断
        /// <summary>
        /// 用户名不能为空
        /// </summary>
        public static string UserNameEmpty = "用户名不能为空!";
        /// <summary>
        /// 用户名不能为空代码
        /// </summary>
        public static string UserNameEmptyCode = "41001";


        /// <summary>
        /// 密码不能为空
        /// </summary>
        public static string PasswordEmpty = "密码不能为空!";
        /// <summary>
        /// 密码不能为空 代码
        /// </summary>
        public static string PasswordEmptyCode = "41002";

        /// <summary>
        /// 用户名或密码错误
        /// </summary>
        public static string UserNamePwdError = "用户名或密码错误!";
        /// <summary>
        /// 用户名或密码错误 代码
        /// </summary>
        public static string UserNamePwdErrorCode = "41005";


        /// <summary>
        /// 用户不存在
        /// </summary>
        public static string UserNotExist = "用户不存在!";
        /// <summary>
        /// 用户不存在代码
        /// </summary>
        public static string UserNotExistCode = "50001";

        /// <summary>
        /// 该用户已存在
        /// </summary>
        public static string UserExist = "用户已存在，请登录或重新注册!";
        /// <summary>
        /// 该用户已存在代码
        /// </summary>
        public static string UserExistCode = "50002";


        /// <summary>
        /// 会员注册失败
        /// </summary>
        public static string UserRegisterFailure = "会员注册失败!";
        /// <summary>
        /// 会员注册失败代码
        /// </summary>
        public static string UserRegisterFailureCode = "50003";



        /// <summary>
        /// 会员或用户已经被锁定或封存
        /// </summary>
        public static string UserLockingSealUp = "用户已经被锁定或封存,请与平台管理员联系!";
        /// <summary>
        /// 会员或用户已经被锁定代码
        /// </summary>
        public static string UserLockingSealUpCode = "50004";


        /// <summary>
        /// 登录连续错误3次
        /// </summary>
        public static string ErrorThree = "您已经连续错误3次,请在{0}分钟后重试!";
        /// <summary>
        /// 登录连续错误3次 代码
        /// </summary>
        public static string ErrorThreeCode = "50005";



        /// <summary>
        /// 该用户 用户状态不允许登录
        /// </summary>
        public static string UserStateNotLogin = "该用户不允许登录,请联系管理员!";
        /// <summary>
        /// 该用户 用户状态不允许登录 代码
        /// </summary>
        public static string UserStateNotLoginCode = "50006";


       

        #endregion

        #region 该用户类型尚无权限

        /// <summary>
        /// 该用户类型尚无权限
        /// </summary>
        public static string UserTypeNotAuthauthCode = "40007!";

        /// <summary>
        /// 该用户类型尚无权限
        /// </summary>
        public static string UserTypeNotAuthauth = "该用户类型尚无权限!";

        #endregion

        #region 操作逻辑判断（增删改查）

        /// <summary>
        /// 查询数据不存在
        /// </summary>
        public static string DataNotExist = "查询数据不存在!";
        /// <summary>
        /// 查询数据不存在代码
        /// </summary>
        public static string DataNotExistCode = "60001";


        /// <summary>
        /// 新增数据失败
        /// </summary>
        public static string CreateError = "新增数据失败!";
        /// <summary>
        /// 新增数据失败代码
        /// </summary>
        public static string CreateErrorCode = "43001";

        /// <summary>
        /// 更新数据失败
        /// </summary>
        public static string UpdateError = "更新数据失败!";
        /// <summary>
        /// 更新数据失败代码
        /// </summary>
        public static string UpdateErrorCode = "43002";

        /// <summary>
        /// 物理删除数据失败
        /// </summary>
        public static string DelError = "删除数据失败!";
        /// <summary>
        /// 物理删除数据失败代码
        /// </summary>
        public static string DelErrorCode = "43003";

        /// <summary>
        /// 内部服务器错误
        /// </summary>
        public static string InternalError = "内部服务器错误!";
        /// <summary>
        /// 内部服务器错误代码
        /// </summary>
        public static string InternalErrorCode = "50000";
        #endregion

        #region Token判断


    

        /// <summary>
        /// 没有令牌进行调用服务，需要登录(401错误)
        /// </summary>
        public static string TokenNotExistCode = "40001";
        /// <summary>
        /// 没有令牌进行调用服务，需要登录(401错误)
        /// </summary>
        //public static string TokenNotExist = "请先登录,再调取该服务!";
        public static string TokenNotExist = "请先登录!";




        /// <summary>
        /// 不合法的凭证类型
        /// </summary>
        public static string TokenErrorCode = "40002";
        /// <summary>
        /// 不合法的凭证类型
        /// </summary>
        //public static string TokenError = "该令牌错误,请重新登录!";
        public static string TokenError = "请重新登录!";


        /// <summary>
        /// 您未被授权使用该功能，请联系管理员进行处理
        /// </summary>
        public static string TokenNotAuthorizeCode = "40003";
        /// <summary>
        /// 您未被授权使用该功能，请重新登录试试或联系管理员进行处理
        /// </summary>
        public static string TokenNotAuthorize = "您未被授权使用该功能,请联系管理员进行处理!";



        /// <summary>
        /// 此权限只能超级管理员拥有
        /// </summary>
        public static string SuperAdminHaveCode = "60006";
        /// <summary>
        /// 此权限只能超级管理员拥有
        /// </summary>
        public static string SuperAdminHave = "此权限只能超级管理员拥有,请联系超级管理员进行处理!";




        /// <summary>
        /// 服务接口不存在,未找到该服务
        /// </summary>
        public static string ServiceNotExistCode = "40004";
        /// <summary>
        /// 服务接口不存在,未找到该服务
        /// </summary>
        public static string ServiceNotExist= "未找到该服务!";




        /// <summary>
        /// 令牌超时或失效,请重新登录
        /// </summary>
        public static string TokenOverTimeCode = "40005";
        /// <summary>
        /// 令牌超时或失效,请重新登录
        /// </summary>
        //public static string TokenOverTime = "令牌超时或失效,请重新登录!";
        public static string TokenOverTime = "请重新登录!";


       


        /// <summary>
        ///超时,请重新登录
        /// </summary>
        public static string OverTime = "40007";
        /// <summary>
        ///超时,请重新登录
        /// </summary>
        public static string OverTimeCode = "超时,请重新登录!";


        /// <summary>
        ///服务错误
        /// </summary>
        public static string ServiceErrorCode = "50000";
        /// <summary>
        ///服务错误
        /// </summary>
        public static string ServiceError = "服务错误!";

        #endregion

        #region 判断当前页，显示条数不能为空
        /// <summary>
        /// 传递参数出现错误
        /// </summary>
        public static string PageError = "当前页不能为空!";
        /// <summary>
        /// 传递参数出现错误代码
        /// </summary>
        public static string PageErrorCode = "40009";

        /// <summary>
        /// 传递参数出现错误
        /// </summary>
        public static string RowError = "显示条数不能为空!";
        /// <summary>
        /// 传递参数出现错误代码
        /// </summary>
        public static string RowErrorCode = "40010";
        #endregion


        #region 无菜单

        /// <summary>
        /// 用户菜单不能为空
        /// </summary>
        public static string UserMensEmpty = "用户菜单为空,请与管理员联系！";
        /// <summary>
        /// 用户菜单不能为空代码
        /// </summary>
        public static string UserMensEmptyCode = "71001";

        #endregion
    }
}
