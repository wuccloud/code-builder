﻿@("/***************************************************")
@("* 模块名称：Repository")
@("* 功能概述：")
@("* 创建人：何贵祥")
@("* 创建时间：" + DateTime.Now.ToString("d") + "")
@("* 修改人：")
@("* 修改时间：")
@("* 修改描述：")
@("*************************************************/")
using Microsoft.Extensions.Configuration;
using SqlSugar;
using System;
using System.Linq;
using @(Model[0].Namespace).IRepository;
//using @(Model[0].Namespace).Model;

namespace @(Model[0].Namespace).Repository
{
    public class SqlSugarRepository:ISqlSugarRepository
    {
        public SqlSugarClient Db { get; set; }
        /// <summary>
        /// 实现SqlSugar对象的仓库(Repository)
        /// </summary>
        /// <returns></returns>
        public SqlSugarRepository()
        {
            Db = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = ConfigExtensions.Configuration.GetConnectionString("TestConnection"),
                DbType =DbType.Sqlite,
                InitKeyType = InitKeyType.Attribute,//从特性读取主键和自增列信息
                IsAutoCloseConnection = true,//开启自动释放模式和EF原理一样我就不多解释了

            }) ;
            //Db.DbFirst.CreateClassFile("c:\\Demo\\1", "test");//生成实体模型Model
            //用来打印Sql方便你调式    
            Db.Aop.OnLogExecuting = (sql, pars) =>
            {
                Console.WriteLine(sql + "\r\n" +
                Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                Console.WriteLine();
            };
            //return Db;
        }

    }
}
