﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using MICZ.JSBD.IRepository.SqlSugar;
using MICZ.JSBD.ViewModels.PageData;

namespace @(Model[0].Namespace).Model
{
    public class @(Model[0].TableName) 
    {
        @foreach(var item in Model)
        {

            @:/// <summary>
            @:/// @(string.IsNullOrWhiteSpace(item.Description)? item.ColumnName.ToUpper() : item.Description)
            @:/// </summary>
            @:public @(item.Type) @(item.ColumnName) { get; set; }
        }
    }

}
