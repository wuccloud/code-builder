# CodeBuilder

#### 介绍
{**CodeBuilder代码生成器**}
CodeBuilder代码生成器是依据[SoEasyPlatform代码生成器](https://gitee.com/sunkaixuan/SoEasyPlatform/)改动而来，UI使用Layui，并结合Sqlsugar支持多种数据库，大家可自行更改/Start/CodeTemplate里面对应的模板，模板使用的是RazorEngine模板引擎。和我们通常使用的Razor语法大同小异。


#### 使用说明

1.从上面的地址下载 CodeBuilder代码生成器到本地

2.解压项目

点击代码生成器-CodeBuilder.sln打开项目

3.配置参数

MICZ.CodeBuilder.Repository项目下的SqlSugarRepository

    Db = new SqlSugarClient(new ConnectionConfig()
    {
        ConnectionString = ConfigExtensions.Configuration.GetConnectionStri("TestConnection"),//连接字符串(使用的是Json配置)　
        DbType =DbType.SqlServer,//数据库类型
        InitKeyType = InitKeyType.Attribute,//从特性读取主键和自增列信息
        IsAutoCloseConnection = true,//开启自动释放模式和EF原理一样我就不多解释
    }) ;

各类数据库连接字符串配置示例#

 :tw-31-20e3: Sqlite：Data Source=./Furion.db

 :tw-32-20e3: MySql：Data Source=localhost;Database=Furion;User ID=root;Password=000000;pooling=true;port=3306;sslmode=none;CharSet=utf8;

 :tw-33-20e3: SqlServer：Server=localhost;Database=Furion;User=sa;Password=000000;MultipleActiveResultSets=True;

 :tw-34-20e3: Oracle：User Id=orcl;Password=orcl;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=orcl)))

 :tw-35-20e3: PostgreSQL：PORT=5432;DATABASE=postgres;HOST=127.0.0.1;PASSWORD=postgres;USER ID=postgres;

4.F5运行

### 如何使用生成的代码开发项目
1.新建一个项目

Web项目或者控制台都可以

2.引用生成的类库(CodeBuilder项目下面的/Start/CodeBuilder文件夹下)

###常见错误
![输入图片说明](常见错误.png "在这里输入图片标题")
双击定位到错误行，删除即可。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
