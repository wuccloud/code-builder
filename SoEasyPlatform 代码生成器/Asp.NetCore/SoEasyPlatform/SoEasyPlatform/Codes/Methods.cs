﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RazorEngine;
using RazorEngine.Templating;

namespace SugarCodeGeneration.Codes
{
    /// <summary>
    /// 生成所需要的代码
    /// </summary>
    public class Methods
    {
        public static Dictionary<string, string> ProjectIds = new Dictionary<string, string>();

        /// <summary>
        /// 获取项目根目录(排除bin\Debug)
        /// </summary>
        public static string GetCurrentProjectPath
        {

            get
            {
                return Environment.CurrentDirectory.Replace(@"\bin\Debug", "").Replace("\\netcoreapp2.0", "");
            }
        }

        /// <summary>
        /// 获取项目根目录完整路径
        /// </summary>
        public static string GetSlnPath
        {

            get
            {
                var path = Directory.GetParent(GetCurrentProjectPath).FullName;
                return path;

            }
        }

        /// <summary>
        /// 添加项目引用
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="refProjectName"></param>
        public static void AddRef(string projectName, string refProjectName)
        {

            var xmlPath = GetSlnPath + @"\" + projectName + @"\" + projectName + ".csproj";

            var xml = File.ReadAllText(xmlPath, System.Text.Encoding.UTF8);
            if (xml.Contains(refProjectName)) return;
            var firstLine = System.IO.File.ReadLines(xmlPath, System.Text.Encoding.UTF8).First();
            var newXml = xml.Replace(firstLine, "").TrimStart('\r').TrimStart('\n');
            XDocument xe = XDocument.Load(xmlPath);
            var root = xe.Root;

            XElement itemGroup = new XElement("ItemGroup");
            var refItem = new XElement("ProjectReference", new XAttribute("Include", string.Format(@"..\{0}\{0}.csproj", refProjectName)));
            refItem.Add(new XElement("Name", refProjectName));
            refItem.Add(new XElement("Project", "{" + ProjectIds[refProjectName] + "}"));
            itemGroup.Add(refItem);
            root.Add(itemGroup);

            newXml = xe.ToString().Replace("xmlns=\"\"", "");
            xe = XDocument.Parse(newXml);
            xe.Save(xmlPath);
        }

        /// <summary>
        /// 创建项目
        /// </summary>
        /// <param name="classPath">项目路径(DbCore|DbModel)</param>
        /// <param name="projectName">项目名称</param>
        public static void AddCsproj(string classPath, string projectName)
        {
            CreateProject(projectName);
            var classDirectory = Methods.GetSlnPath + "\\" + projectName + "\\" + classPath.TrimStart('\\');
            if (FileHelper.IsExistDirectory(classDirectory) == false)
            {
                FileHelper.CreateDirectory(classDirectory);
            }
            var files = Directory.GetFiles(classDirectory).ToList().Select(it => classPath + "\\" + Path.GetFileName(it));
            var xmlPath = GetSlnPath + @"\" + projectName + @"\" + projectName + ".csproj";

            var xml = File.ReadAllText(xmlPath, System.Text.Encoding.UTF8);
            var firstLine = System.IO.File.ReadLines(xmlPath, System.Text.Encoding.UTF8).First();
            var newXml = xml.Replace(firstLine, "").TrimStart('\r').TrimStart('\n');
            XDocument xe = XDocument.Load(xmlPath);
            //var itemGroup = xe.Root.Elements().Where(it => it.Name.LocalName == "ItemGroup" && it.Elements().Any(y => y.Name.LocalName == "Compile")).First();
            //var compieList = itemGroup.Elements().ToList();
            //var noAddFiles = files.Where(it => !compieList.Any(f => it.Equals(f.Attribute("Include").Value, StringComparison.CurrentCultureIgnoreCase))).ToList();
            //if (noAddFiles.Any())
            //{
            //    foreach (var item in noAddFiles)
            //    {
            //        var addItem = new XElement("Compile", new XAttribute("Include", item.TrimStart('\\')));
            //        itemGroup.AddFirst(addItem);
            //    }
            //}
            newXml = xe.ToString().Replace("xmlns=\"\"", "");
            xe = XDocument.Parse(newXml);
            xe.Save(xmlPath);
        }

        /// <summary>
        /// 修改解决方案
        /// </summary>
        /// <param name="solutionName">解决方案名称</param>
        public static void RenameSln(string solutionName)
        {
            var slns = Directory.GetFiles(GetSlnPath).Where(it => it.Contains(".sln"));
            if (slns.Any())
            {
                File.Move(slns.First(), GetSlnPath + "\\" + solutionName + ".sln");
            }
        }

        /// <summary>
        /// 根据模板创建BLL文件
        /// </summary>
        /// <param name="templatePath">模板路径</param>
        /// <param name="savePath">保存文件路径</param>
        /// <param name="tables">数据库表名List</param>
        /// <param name="classNamespace">项目命名空间</param>
        public static void CreateBLL(string templatePath, string savePath, List<string> tables, string classNamespace)
        {

            string template = System.IO.File.ReadAllText(templatePath); //从文件中读出模板内容
            string templateKey = "bll"; //取个名字
            foreach (var item in tables)
            {
                BLLParameter model = new BLLParameter()
                {
                    Name = item,
                    ClassNamespace = classNamespace
                };
                var result = Engine.Razor.RunCompile(template, templateKey, model.GetType(), model);
                var cp = savePath + "\\" + item + "Manager.cs";
                if (FileHelper.IsExistFile(cp) == false)
                    FileHelper.CreateFile(cp, result, System.Text.Encoding.UTF8);
            }
        }

        /// <summary>
        /// 根据模板创建DbContext文件
        /// </summary>
        /// <param name="templatePath">模板路径</param>
        /// <param name="savePath">保存文件路径</param>
        /// <param name="model">模型</param>
        public static void CreateDbContext(string templatePath, string savePath, object model)
        {
            string template = System.IO.File.ReadAllText(templatePath); //从文件中读出模板内容
            string templateKey = "dbcontext"; //取个名字
            var result = Engine.Razor.RunCompile(template, templateKey, model.GetType(), model);
            FileHelper.CreateFile(savePath, result, System.Text.Encoding.UTF8);
        }

        /// <summary>
        /// 创建项目(.csproj)
        /// </summary>
        /// <param name="name">项目名称</param>
        public static void CreateProject(string name)
        {
            var templatePath = GetCurrentProjectPath + "/Template/Project.txt";
            string projectId = Guid.NewGuid().ToString();
            string project = System.IO.File.ReadAllText(templatePath).Replace("@pid", projectId).Replace("@AssemblyName", name); //从文件中读出模板内容
            var projectPath = GetSlnPath + "\\" + name + "\\" + name + ".csproj";
            var projectDic = GetSlnPath + "\\" + name + "\\";
            var binDic = GetSlnPath + "\\" + name + "\\bin";
            if (!FileHelper.IsExistFile(projectPath))
            {

                if (!FileHelper.IsExistDirectory(projectDic))
                {
                    FileHelper.CreateDirectory(projectDic);
                }
                if (!FileHelper.IsExistDirectory(binDic))
                {
                    FileHelper.CreateDirectory(binDic);
                }
                FileHelper.CreateFile(projectPath, project, System.Text.Encoding.UTF8);
                //FileHelper.CreateFile(projectDic + "\\class1.cs", "", System.Text.Encoding.UTF8);
                File.Copy(GetCurrentProjectPath + "/Template/nuget.txt", projectDic + "packages.config");
                ProjectIds.Add(name, projectId);
                AppendProjectToSln(projectId, name);
            }
        }

        /// <summary>
        /// 添加项目引用到解决方案(.sln)
        /// </summary>
        /// <param name="projectId">项目id</param>
        /// <param name="projectName">项目名称</param>
        public static void AppendProjectToSln(string projectId, string projectName)
        {

            var slns = Directory.GetFiles(GetSlnPath).Where(it => it.Contains(".sln"));
            if (slns.Any())
            {
                var sln = slns.First();
                var templatePath = GetCurrentProjectPath + "/Template/sln.txt";
                string appendText = System.IO.File.ReadAllText(templatePath)
                    .Replace("@pid", projectId)
                    .Replace("@name", projectName)
                    .Replace("@sid", Guid.NewGuid().ToString());
                FileStream fs = new FileStream(sln, FileMode.Append);
                var sw = new StreamWriter(fs);
                sw.WriteLine(appendText);
                sw.Close();
                fs.Close();
            }

        }
    }
}
