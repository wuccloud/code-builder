﻿using Sugar.Enties;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
public class DbContext<T> where T : class, new()
{
    public DbContext()
    {
        Db = new SqlSugarClient(new ConnectionConfig()
        {
            ConnectionString = "server=.;uid=sa;pwd=123;database=netcoredev",
            DbType = DbType.SqlServer,
            InitKeyType = InitKeyType.Attribute,//从特性读取主键和自增列信息
            IsAutoCloseConnection = true,//开启自动释放模式和EF原理一样我就不多解释了

        });
        //调式代码 用来打印SQL 
        Db.Aop.OnLogExecuting = (sql, pars) =>
        {
            Console.WriteLine(sql + "\r\n" +
                Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
            Console.WriteLine();
        };

    }
    //注意：不能写成静态的
    public SqlSugarClient Db;//用来处理事务多表查询和复杂的操作
	public SimpleClient<T> CurrentDb { get { return new SimpleClient<T>(Db); } }//用来操作当前表的数据

   public SimpleClient<App_Appointment> App_AppointmentDb { get { return new SimpleClient<App_Appointment>(Db); } }//用来处理App_Appointment表的常用操作
   public SimpleClient<App_Appointment1> App_Appointment1Db { get { return new SimpleClient<App_Appointment1>(Db); } }//用来处理App_Appointment1表的常用操作
   public SimpleClient<App_Expert> App_ExpertDb { get { return new SimpleClient<App_Expert>(Db); } }//用来处理App_Expert表的常用操作
   public SimpleClient<App_News> App_NewsDb { get { return new SimpleClient<App_News>(Db); } }//用来处理App_News表的常用操作
   public SimpleClient<App_ReportPrice> App_ReportPriceDb { get { return new SimpleClient<App_ReportPrice>(Db); } }//用来处理App_ReportPrice表的常用操作
   public SimpleClient<App_Transaction> App_TransactionDb { get { return new SimpleClient<App_Transaction>(Db); } }//用来处理App_Transaction表的常用操作
   public SimpleClient<App_TransactionAvgPrice> App_TransactionAvgPriceDb { get { return new SimpleClient<App_TransactionAvgPrice>(Db); } }//用来处理App_TransactionAvgPrice表的常用操作
   public SimpleClient<Sell8> Sell8Db { get { return new SimpleClient<Sell8>(Db); } }//用来处理Sell8表的常用操作
   public SimpleClient<SellOrder> SellOrderDb { get { return new SimpleClient<SellOrder>(Db); } }//用来处理SellOrder表的常用操作
   public SimpleClient<SellOrderList> SellOrderListDb { get { return new SimpleClient<SellOrderList>(Db); } }//用来处理SellOrderList表的常用操作
   public SimpleClient<Sys_City> Sys_CityDb { get { return new SimpleClient<Sys_City>(Db); } }//用来处理Sys_City表的常用操作
   public SimpleClient<Sys_Dictionary> Sys_DictionaryDb { get { return new SimpleClient<Sys_Dictionary>(Db); } }//用来处理Sys_Dictionary表的常用操作
   public SimpleClient<Sys_DictionaryList> Sys_DictionaryListDb { get { return new SimpleClient<Sys_DictionaryList>(Db); } }//用来处理Sys_DictionaryList表的常用操作
   public SimpleClient<Sys_Log> Sys_LogDb { get { return new SimpleClient<Sys_Log>(Db); } }//用来处理Sys_Log表的常用操作
   public SimpleClient<Sys_Menu> Sys_MenuDb { get { return new SimpleClient<Sys_Menu>(Db); } }//用来处理Sys_Menu表的常用操作
   public SimpleClient<Sys_Province> Sys_ProvinceDb { get { return new SimpleClient<Sys_Province>(Db); } }//用来处理Sys_Province表的常用操作
   public SimpleClient<Sys_Role> Sys_RoleDb { get { return new SimpleClient<Sys_Role>(Db); } }//用来处理Sys_Role表的常用操作
   public SimpleClient<Sys_RoleAuth> Sys_RoleAuthDb { get { return new SimpleClient<Sys_RoleAuth>(Db); } }//用来处理Sys_RoleAuth表的常用操作
   public SimpleClient<Sys_RoleAuthData> Sys_RoleAuthDataDb { get { return new SimpleClient<Sys_RoleAuthData>(Db); } }//用来处理Sys_RoleAuthData表的常用操作
   public SimpleClient<Sys_TableColumn> Sys_TableColumnDb { get { return new SimpleClient<Sys_TableColumn>(Db); } }//用来处理Sys_TableColumn表的常用操作
   public SimpleClient<Sys_TableInfo> Sys_TableInfoDb { get { return new SimpleClient<Sys_TableInfo>(Db); } }//用来处理Sys_TableInfo表的常用操作
   public SimpleClient<Sys_User> Sys_UserDb { get { return new SimpleClient<Sys_User>(Db); } }//用来处理Sys_User表的常用操作
   public SimpleClient<test2019> test2019Db { get { return new SimpleClient<test2019>(Db); } }//用来处理test2019表的常用操作
   public SimpleClient<test2019xxx> test2019xxxDb { get { return new SimpleClient<test2019xxx>(Db); } }//用来处理test2019xxx表的常用操作


   /// <summary>
    /// 获取所有
    /// </summary>
    /// <returns></returns>
    public virtual List<T> GetList()
    {
        return CurrentDb.GetList();
    }

    /// <summary>
    /// 根据表达式查询
    /// </summary>
    /// <returns></returns>
    public virtual List<T> GetList(Expression<Func<T, bool>> whereExpression)
    {
        return CurrentDb.GetList(whereExpression);
    }


    /// <summary>
    /// 根据表达式查询分页
    /// </summary>
    /// <returns></returns>
    public virtual List<T> GetPageList(Expression<Func<T, bool>> whereExpression, PageModel pageModel)
    {
        return CurrentDb.GetPageList(whereExpression, pageModel);
    }

    /// <summary>
    /// 根据表达式查询分页并排序
    /// </summary>
    /// <param name="whereExpression">it</param>
    /// <param name="pageModel"></param>
    /// <param name="orderByExpression">it=>it.id或者it=>new{it.id,it.name}</param>
    /// <param name="orderByType">OrderByType.Desc</param>
    /// <returns></returns>
    public virtual List<T> GetPageList(Expression<Func<T, bool>> whereExpression, PageModel pageModel, Expression<Func<T, object>> orderByExpression = null, OrderByType orderByType = OrderByType.Asc)
    {
        return CurrentDb.GetPageList(whereExpression, pageModel,orderByExpression,orderByType);
    }


    /// <summary>
    /// 根据主键查询
    /// </summary>
    /// <returns></returns>
    public virtual T GetById(dynamic id)
    {
        return CurrentDb.GetById(id);
    }

    /// <summary>
    /// 根据主键删除
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Delete(dynamic id)
    {
        return CurrentDb.Delete(id);
    }


    /// <summary>
    /// 根据实体删除
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Delete(T data)
    {
        return CurrentDb.Delete(data);
    }

    /// <summary>
    /// 根据主键删除
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Delete(dynamic[] ids)
    {
        return CurrentDb.AsDeleteable().In(ids).ExecuteCommand()>0;
    }

    /// <summary>
    /// 根据表达式删除
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Delete(Expression<Func<T, bool>> whereExpression)
    {
        return CurrentDb.Delete(whereExpression);
    }


    /// <summary>
    /// 根据实体更新，实体需要有主键
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Update(T obj)
    {
        return CurrentDb.Update(obj);
    }

    /// <summary>
    ///批量更新
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Update(List<T> objs)
    {
        return CurrentDb.UpdateRange(objs);
    }

    /// <summary>
    /// 插入
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Insert(T obj)
    {
        return CurrentDb.Insert(obj);
    }


    /// <summary>
    /// 批量
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool Insert(List<T> objs)
    {
        return CurrentDb.InsertRange(objs);
    }


    //自已扩展更多方法 
}


